/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.efrei;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 *
 * @author Gator
 */
public class Constantes {
    Connection conn;
    Statement stmt;
    PreparedStatement pstmt;
    ResultSet results;
    Properties dbProperties;
    String dbUrl;
    String dbUser;
    String dbPwd;
    
    public Constantes() {
        conn = null;
        stmt = null;
        results = null;
        dbProperties = new Properties();
    }
    
    public void initProperties() throws IOException {
        dbProperties.load(getClass().getClassLoader().getResourceAsStream("utils/db.properties"));
        dbUrl = dbProperties.getProperty("dbUrl");
        dbUser = dbProperties.getProperty("dbUser");
        dbPwd = dbProperties.getProperty("dbPwd");
    }
    
    public void initConn() throws SQLException {
        //conn = DriverManager.getConnection(dbUrl + ";user=" + dbUser + ";password=" + dbPwd);
        try {
        Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Cannot find the driver in the classpath!", e);
        }
        conn = DriverManager.getConnection(dbUrl, dbUser, dbPwd);
    }
    
    public ResultSet execute(String sql) throws SQLException {
        stmt = conn.createStatement();
        results = stmt.executeQuery(sql);
        return results;
    }
    
    public void delete(int id) throws SQLException {
        pstmt = conn.prepareStatement("delete from EMPLOYES where id = ? ");
        pstmt.setInt(1, id);
        pstmt.executeUpdate();
    }
    
    public void update(Employe e) throws SQLException {
        String sql;
        sql = "update EMPLOYES "
                + "set nom = ?, "
                + "prenom = ?, "
                + "teldom = ?, "
                + "telport = ?, "
                + "telpro = ?, "
                + "adresse = ?, "
                + "codepostal = ?, "
                + "ville = ?, "
                + "email = ? "
                + "where id = ? ";
        
        pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, e.getNom());
        pstmt.setString(2, e.getPrenom());
        pstmt.setString(3, e.getTelDom());
        pstmt.setString(4, e.getTelPort());
        pstmt.setString(5, e.getTelPro());
        pstmt.setString(6, e.getAdresse());
        pstmt.setString(7, e.getCodePostal());
        pstmt.setString(8, e.getVille());
        pstmt.setString(9, e.getEmail());
        pstmt.setInt(10, e.getId());
        
        pstmt.executeUpdate();
    }
    
    public void add(Employe e) throws SQLException {
        String sql;
        sql = "insert into employes (nom,prenom,teldom,telport,telpro,adresse,codepostal,ville,email) values (?,?,?,?,?,?,?,?,?)";
        
        pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, e.getNom());
        pstmt.setString(2, e.getPrenom());
        pstmt.setString(3, e.getTelDom());
        pstmt.setString(4, e.getTelPort());
        pstmt.setString(5, e.getTelPro());
        pstmt.setString(6, e.getAdresse());
        pstmt.setString(7, e.getCodePostal());
        pstmt.setString(8, e.getVille());
        pstmt.setString(9, e.getEmail());
        pstmt.executeUpdate();
    }
}

