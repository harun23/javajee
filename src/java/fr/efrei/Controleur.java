/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.efrei;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Gator
 */
public class Controleur extends HttpServlet {

    private Constantes cs;
    private String message;
    private String nomPage;
    private String nextPage;
    private ArrayList<Employe> employes;
    private boolean deconnexion;

    public Controleur() {
        message = null;
        nextPage = "";
        nomPage = "Login";
        cs = new Constantes();
        employes = new ArrayList<>();
        deconnexion = false;
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);

        String action = request.getParameter("action");

        if (action == null) {
            String login = request.getParameter("login");
            String mdp = request.getParameter("mdp");
            String connect = "Votre session est active";
            session.setAttribute("nompage", nomPage);
            nextPage = "/WEB-INF/index.jsp";

            try {
                cs.initProperties();
                cs.initConn();
            } catch (SQLException ex) {
                Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (login != null && mdp != null) {
                if (login.equals("") || mdp.equals("")) {
                    message = "Vous devez renseigner les deux champs";
                } else if (login.equals("admin") && mdp.equals("admin")) {
                    nomPage = "Dashboard";
                    session.setAttribute("nompage", nomPage);
                    deconnexion = true;
                    session.setAttribute("deconnexion", deconnexion);
                    nextPage = "/WEB-INF/dashboard.jsp";
                    initDashboard(cs);
                    session.setAttribute("connect", connect);
                    session.setAttribute("employes", employes);
                } else {
                    message = "Echec de connexion ! Vérifiez votre login et/ou mot de passe et essayez à nouveau";
                }
            }
        } else if (action.equals("ajouter")) {
            nomPage = "Ajouter";
            session.setAttribute("nompage", nomPage);
            nextPage = "/WEB-INF/add.jsp";
        } else if (action.equals("details")) {
            nomPage = "Détails";
            session.setAttribute("nompage", nomPage);
            nextPage = "/WEB-INF/update.jsp";
            int id = Integer.parseInt(request.getParameter("radios"));
            for (Employe e : employes) {
                if (e.getId() == id) {
                    session.setAttribute("selectedEmploye", e);
                    break;
                }
            }
        } else if (action.equals("supprimer")) {
            String id = request.getParameter("radios");
            message = delete(Integer.parseInt(id));
        } else if (action.equals("deconnexion")) {
            message = "Vous êtes déconnecté";
            nextPage = "/WEB-INF/index.jsp";
            deconnexion = false;
            session.setAttribute("connect", null);
            session.setAttribute("deconnexion", deconnexion);

        } else if (action.equals("modifier")) {
            Employe e = (Employe) session.getAttribute("selectedEmploye");

            e.setNom(request.getParameter("nom"));
            e.setPrenom(request.getParameter("prenom"));
            e.setTelDom(request.getParameter("teldom"));
            e.setTelPort(request.getParameter("telport"));
            e.setTelPro(request.getParameter("telpro"));
            e.setAdresse(request.getParameter("adresse"));
            e.setCodePostal(request.getParameter("cp"));
            e.setVille(request.getParameter("ville"));
            e.setEmail(request.getParameter("email"));

            update(e);
            message = "Les informations de l'employé ont été modifiés!";
            initDashboard(cs);
            nextPage = "/WEB-INF/update.jsp";
        } else if (action.equals("retour")) {
            nomPage = "Dashboard";
            session.setAttribute("nompage", nomPage);
            nextPage = "/WEB-INF/dashboard.jsp";
            initDashboard(cs);
        } else if (action.equals("ajout")) {
            Employe e = new Employe();

            e.setNom(request.getParameter("nom"));
            e.setPrenom(request.getParameter("prenom"));
            e.setTelDom(request.getParameter("teldom"));
            e.setTelPort(request.getParameter("telport"));
            e.setTelPro(request.getParameter("telpro"));
            e.setAdresse(request.getParameter("adresse"));
            e.setCodePostal(request.getParameter("cp"));
            e.setVille(request.getParameter("ville"));
            e.setEmail(request.getParameter("email"));

            employes.add(e);
            add(e);
            message = "Employé a été ajouté !!";

            initDashboard(cs);
            nextPage = "/WEB-INF/add.jsp";
        }

        session.setAttribute("msg", message);
        message = null;

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextPage);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void initDashboard(Constantes cs) {
        employes.clear();
        ResultSet rs;
        try {
            rs = cs.execute("select * from employes");
            while (rs.next()) {
                Employe e = new Employe();
                e.setId(rs.getInt("Id"));
                e.setNom(rs.getString("Nom"));
                e.setPrenom(rs.getString("Prenom"));
                e.setTelDom(rs.getString("Teldom"));
                e.setTelPort(rs.getString("Telport"));
                e.setTelPro(rs.getString("Telpro"));
                e.setAdresse(rs.getString("Adresse"));
                e.setCodePostal(rs.getString("Codepostal"));
                e.setVille(rs.getString("Ville"));
                e.setEmail(rs.getString("Email"));
                employes.add(e);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private String delete(int id) {
        String reponse = "La suppression a réussi !";
        try {
            cs.delete(id);
        } catch (SQLException ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
            reponse = "La suppression a échoué";
        }

        for (Employe e : employes) {
            if (e.getId() == id) {
                employes.remove(e);
                break;
            }
        }
        return reponse;
    }

    private void update(Employe e) {
        try {
            cs.update(e);
        } catch (SQLException ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void add(Employe e) {
        try {
            cs.add(e);
        } catch (SQLException ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
