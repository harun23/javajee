/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.efrei;

/**
 *
 * @author Gator
 */
public class Employe {
    private int id;
    private String nom;
    private String prenom;
    private String telDom;
    private String telPort;
    private String telPro;
    private String adresse;
    private String codePostal;
    private String ville;
    private String email;
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the telDom
     */
    public String getTelDom() {
        return telDom;
    }

    /**
     * @param telDom the telDom to set
     */
    public void setTelDom(String telDom) {
        this.telDom = telDom;
    }

    /**
     * @return the telPort
     */
    public String getTelPort() {
        return telPort;
    }

    /**
     * @param telPort the telPort to set
     */
    public void setTelPort(String telPort) {
        this.telPort = telPort;
    }

    /**
     * @return the telPro
     */
    public String getTelPro() {
        return telPro;
    }

    /**
     * @param telPro the telPro to set
     */
    public void setTelPro(String telPro) {
        this.telPro = telPro;
    }

    /**
     * @return the adresse
     */
    public String getAdresse() {
        return adresse;
    }

    /**
     * @param adresse the adresse to set
     */
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    /**
     * @return the codePostal
     */
    public String getCodePostal() {
        return codePostal;
    }

    /**
     * @param codePostal the codePostal to set
     */
    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * @return the ville
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param ville the ville to set
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
