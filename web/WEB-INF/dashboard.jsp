<%-- 
    Document   : dashboard
    Created on : 23 nov. 2018, 09:46:31
    Author     : sumac
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">Dashboard</h1>
    </div>
</section>
<c:if test="${msg!=null}">
    <div class="alert alert-success" role="alert">
        <h3><c:out value="${msg}" /></h3>
    </div>
</c:if>

<form action="Controleur" method="post">
    <c:choose>
        <c:when test="${employes.isEmpty()}">
            <div class="alert alert-info" role="alert">
                Nous devons recruter !
            </div>
        </c:when> 
        <c:otherwise>
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <tr>
                        <th scope="col">Sél</th>
                        <th scope="col">NOM</th>
                        <th scope="col">PRENOM</th>
                        <th scope="col">TEL DOMICILE</th>
                        <th scope="col">TEL MOBILE</th>
                        <th scope="col">TEL PRO</th>
                        <th scope="col">ADRESSE</th>
                        <th scope="col">CODE POSTAL</th>
                        <th scope="col">VILLE</th>
                        <th scope="col">EMAIL</th>
                    </tr>
                    <c:forEach items="${employes}" var="e">
                        <tr>
                            <td><input type="radio" name="radios" value="${e.id}" checked/> </td>
                            <td><c:out value="${e.nom}" /> </td>
                            <td><c:out value="${e.prenom}" /> </td>
                            <td><c:out value="${e.telDom}" /> </td>
                            <td><c:out value="${e.telPort}" /> </td>
                            <td><c:out value="${e.telPro}" /> </td>
                            <td><c:out value="${e.adresse}" /> </td>
                            <td><c:out value="${e.codePostal}" /> </td>
                            <td><c:out value="${e.ville}" /> </td>
                            <td><c:out value="${e.email}" /> </td>
                        </tr>
                    </c:forEach>              
                </table>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="form-group row">
        <div class="col-sm">
            <button class="btn  btn-lg btn-success btn-block" type="submit" name="action" value="ajouter">Ajouter</button>
        </div>
        <c:choose>
            <c:when test="${employes.isEmpty()}">

            </c:when>    
            <c:otherwise>
                <div class="col-sm">
                    <button class="btn btn-lg btn-primary btn-block" type="submit" name="action" value="details">Détails</button>
                </div>
                <div class="col-sm">
                    <button class="btn btn-lg btn-danger btn-block" type="submit" name="action" value="supprimer">Supprimer</button>
                </div>
            </c:otherwise>
        </c:choose>

    </div>
</form>
</main>
</body>
</html>
