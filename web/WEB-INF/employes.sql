/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Gator
 * Created: 23 nov. 2018
 */

/**** CIBLE : Java DB ****/

/*On supprime les tables si elles existent */
DROP TABLE EMPLOYES;

/*Cr�ation de la table EMPLOYES*/
CREATE TABLE EMPLOYES (
	ID INT AUTO_INCREMENT,
	NOM VARCHAR(25) NOT NULL,
	PRENOM VARCHAR(25) NOT NULL,
	TELDOM VARCHAR(10) NOT NULL,
	TELPORT VARCHAR(10) NOT NULL,
	TELPRO VARCHAR(10) NOT NULL,
	ADRESSE VARCHAR(150) NOT NULL,
	CODEPOSTAL VARCHAR(5) NOT NULL,
	VILLE VARCHAR(25) NOT NULL,
	EMAIL VARCHAR(25) NOT NULL,
	CONSTRAINT primary_key_membre PRIMARY KEY (ID)
);

/*Insertion de 4 membres*/
INSERT INTO EMPLOYES(NOM,PRENOM,TELDOM,TELPORT,TELPRO,ADRESSE,CODEPOSTAL,VILLE,EMAIL) VALUES
('Turing','Alan','0123456789','0612345678','0698765432','2 rue des Automates','92700','Colombes','aturing@efrei.fr'),
('Galois','Evariste','0145362787','0645362718','0611563477','21 rue des Morts-trop-jeunes','92270','Bois-colombes','egalois@efrei.fr'),
('Boole','George','0187665987','0623334256','0654778654','65 rue de la Logique','92700','Colombes','gboole@efrei.fr'),
('Gauss','Carl Friedrich','0187611987','0783334256','0658878654','6 rue des Transformations','75016','Paris','cgauss@efrei.fr'),
('Pascal','Blaise','0187384987','0622494256','0674178654','39 bvd de Port-Royal','21000','Dijon','bpascal@efrei.fr'),
('Euler','Leonhard','0122456678','0699854673','0623445166','140 avenue Complexe','90000','Nanterre','leuler@efrei.fr'),
('Einstein','Albert','0123458678','0655854673','0623335166','14 av Jean-Jacques Rousseau','91100','Evry','aeinstein@efrei.fr'),
('Suicer','Ayetullah','0128956678','0699823673','0623475166','5 rue Pacha','93250','Istanbul','asuicer@efrei.fr'),
('Mourougappa','Nandane','0122456858','0699214673','0623415166','21 bvd Raja','45850','Pondichéry','nmourougappa@efrei.fr'),
('Hamidov','Ali','0111456678','0677854673','0623446276','500 bvd Loup','91700','Grozny','ahamidov@efrei.fr');
