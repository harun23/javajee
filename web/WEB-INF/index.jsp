<%-- 
    Document   : index
    Created on : 23 nov. 2018, 09:30:39
    Author     : Gator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form action="Controleur" method="post" class="form-signin">
    <c:if test="${msg!=null}">
        <div class="alert alert-danger" role="alert">
            <c:out value="${msg}" />
        </div>
    </c:if>
    <h1 class="h3 mb-3 font-weight-normal">Connexion</h1>
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" class="form-control" id="login" name="login" placeholder="Login">
    </div>
    <div class="form-group">
        <label for="mdp">Mot de passse</label>
        <input type="password" class="form-control" id="mdp" name="mdp" placeholder="Mot de passe">
    </div>
    <button type="submit" class="btn btn-lg btn-primary btn-block" >Login</button>
</form>
</main>

</body>
</html>
