<%-- 
    Document   : add
    Created on : 23 nov. 2018, 11:09:08
    Author     : sumac
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">Détails</h1>
    </div>
</section>
<c:if test="${msg!=null}">
    <div class="alert alert-success" role="alert">
        <h3><c:out value="${msg}" /></h3>
    </div>
</c:if>
<form method="post" action="Controleur">
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Nom</label>
        <div class="col-sm-9">
            <input type="text" name="nom" class="form-control" value="${selectedEmploye.nom}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Prenom</label>
        <div class="col-sm-9">
            <input type="text" name="prenom" class="form-control" value="${selectedEmploye.prenom}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Tél Dom</label>
        <div class="col-sm-9">
            <input type="text" name="teldom" class="form-control" value="${selectedEmploye.telDom}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Tél Mob</label>
        <div class="col-sm-9">
            <input type="text" name="telport" class="form-control" value="${selectedEmploye.telPort}">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Tél Pro</label>
        <div class="col-sm-9">
            <input type="text" name="telpro" class="form-control" value="${selectedEmploye.telPro}">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>Adresse</label>
            <input type="text" name="adresse" class="form-control" value="${selectedEmploye.adresse}">
        </div>
        <div class="form-group col-md-6">
            <label>Code Postal</label>
            <input type="text" name="cp" class="form-control" value="${selectedEmploye.codePostal}">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label>Ville</label>
            <input type="text" name="ville" class="form-control" value="${selectedEmploye.ville}">
        </div>
        <div class="form-group col-md-6">
            <label>Adresse e-mail</label>
            <input type="text" name="email" class="form-control" value="${selectedEmploye.email}">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-4">
            <button type="submit" class="btn btn-lg btn-primary btn-block" name="action" value="modifier">Modifier</button>
        </div>
        <div class="col-sm-4">
            <button type="submit" class="btn btn-lg btn-primary btn-block" name="action" value="retour">Voir la liste</button>
        </div>
</form>
</body>
</html>
